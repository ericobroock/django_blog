FROM python:latest
WORKDIR /blog
COPY . .
RUN python -m venv myvenv
RUN . myvenv/bin/activate
RUN ls -la
RUN pip install -r requirements.txt
RUN ls mysite
RUN python mysite/manage.py migrate
RUN python mysite/manage.py runserver 0.0.0.0:8000 &